import chalk from 'chalk';
import { end_point_db } from '../server';
import { isUserHaveSearches, isUserCanSearchThisLimit, generateApiError, isReqWithoutFilters } from '../utils';
import { generateConditionsToAgrFactor, generateQueryToAgrFactor, generateAgrStats } from '../dbUtils/statistics/agrFactorStats';
import { generateQueryToPreCompiledResults, preparePreCompiledResults, generateQueryToCompiledResults, prepareCompiledResults } from '../dbUtils/statistics/baseStatsUtils';
import { generateQueryToGraphStats, prepareGraphResults, generateConditionsToGraphStats } from '../dbUtils/statistics/graphStats';


async function checkPermissions(user, playername, pokersite_id, limit) {
    if (!playername || !pokersite_id) {
        return Promise.reject(generateApiError('Pls enter nickname and pokersite_id.', 400));
    }
    const player = await end_point_db.one(
        'select * from players where normalized_playername = $1 and pokersite_id = $2',
        [playername.toLowerCase(), pokersite_id],
    ).catch((err) => {
        console.log(chalk.red(new Date().toString()), 'get Agr factor', err);
        return Promise.reject(generateApiError('No players with such nickname.', 400));
    });

    if (!user.is_admin) {
        if (!user.is_unlimit && !isUserHaveSearches(user.number_of_searches)) {
            return Promise.reject(generateApiError('Your limit is over for today.', 403));
        }

        if (!isUserCanSearchThisLimit(limit, user.subscription_type, player.home_limit)) {
            return Promise.reject(generateApiError('You cant search this limit.', 403));
        }
    }
    return Promise.resolve(player);
}

async function getAgrFactorStats(nickname, pokerGameType, limit, tableSize, date, pokersite_id, player) {
    let agrStatsFromCash;
    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        agrStatsFromCash = await end_point_db.one(
            'select stats from agr_factor_stats_cash where playername = $1 and pokersite_id = $2',
            [player.playername, player.pokersite_id],
        ).catch((err) => { console.log(chalk.red(new Date().toString()), 'get Agr factor cash', err); });
    }
    if (agrStatsFromCash) {
        return Promise.resolve({ player, agrStats: JSON.parse(agrStatsFromCash.stats) });
    }

    const conditions = generateConditionsToAgrFactor(nickname, pokerGameType, limit, tableSize, date, pokersite_id);
    const additionalTables = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'additionalplayerresults%\'')
        .catch(() => Promise.reject(generateApiError('No aggresion results to this request', 404)));
    const query = generateQueryToAgrFactor(additionalTables, conditions, player);
    const stats = await end_point_db.one(query.query, query.params)
        .catch(() => Promise.reject(generateApiError('No aggresion results to this request', 404)));

    const agrStats = generateAgrStats(stats);

    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        await end_point_db.any(
            'insert into agr_factor_stats_cash (playername, pokersite_id, stats) values($1, $2, $3)'
            , [player.playername, player.pokersite_id, JSON.stringify(agrStats)],
        );
    }

    return Promise.resolve({ player, agrStats });
}

async function getBaseStats(nickname, pokerGameType, limit, tableSize, date, pokersite_id, player) {
    let baseStatsFromCash;
    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        baseStatsFromCash = await end_point_db.one(
            'select stats from base_stats_cash where playername = $1 and pokersite_id = $2',
            [player.playername, player.pokersite_id],
        ).catch((err) => { console.log(chalk.red(new Date().toString()), 'get basestats cash', err); });
    }
    if (baseStatsFromCash) {
        return Promise.resolve(Object.assign({}, { player }, JSON.parse(baseStatsFromCash.stats)));
    }

    let results;
    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        const preCompiledTables = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'pre_mycompiledplayerresults%\'');
        const query = generateQueryToPreCompiledResults(preCompiledTables, player);

        const stats = await end_point_db.many(query.query, query.params)
            .catch(() => Promise.reject(generateApiError('No results to this request', 404)));

        results = preparePreCompiledResults(stats);
    } else {
        const compiledTables = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');
        const conditions = generateConditionsToAgrFactor(nickname, pokerGameType, limit, tableSize, date, pokersite_id);
        const query = generateQueryToCompiledResults(compiledTables, conditions, player);
        results = await end_point_db.many(query.query, query.params)
            .catch(() => Promise.reject(generateApiError('No results for this request.', 404)));
    }

    const { stats, monthResults } = prepareCompiledResults(results, player.playername);

    if (stats.hands === 0) {
        Promise.reject(generateApiError('No results for this request.', 404));
    }

    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        await end_point_db.any(
            'insert into base_stats_cash (playername, pokersite_id, stats) values($1, $2, $3)'
            , [player.playername, player.pokersite_id, JSON.stringify({
                stats, monthResults, home: player.home_limit,
            })],
        );
    }

    return Promise.resolve({
        player, stats, monthResults, home: player.home_limit,
    });
}

async function getGraphStats(nickname, pokerGameType, limit, tableSize, date, pokersite_id, player) {
    let graphStatsFromCash;
    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        graphStatsFromCash = await end_point_db.one(
            'select stats from graph_cash where playername = $1 and pokersite_id = $2',
            [player.playername, player.pokersite_id],
        ).catch((err) => { console.log(chalk.red(new Date().toString()), 'get graph cash', err); });
    }
    if (graphStatsFromCash) {
        return Promise.resolve({ player, graphStats: JSON.parse(graphStatsFromCash.stats) });
    }

    const formatedTables = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'');
    const conditions = generateConditionsToGraphStats(nickname, pokerGameType, limit, tableSize, date, pokersite_id);
    const query = generateQueryToGraphStats(formatedTables, conditions, player);

    const graph = await end_point_db.many(query.query, query.params)
        .catch(() => Promise.reject(generateApiError('No stats results for this request.', 404)));


    const graphStats = prepareGraphResults(graph);
    if (isReqWithoutFilters(pokerGameType, limit, tableSize, date)) {
        await end_point_db.any(
            'insert into graph_cash (playername, pokersite_id, stats) values($1, $2, $3)'
            , [player.playername, player.pokersite_id, JSON.stringify(graphStats)],
        );
    }

    return Promise.resolve({ player, graphStats });
}

export default {
    checkPermissions,
    getAgrFactorStats,
    getBaseStats,
    getGraphStats,
};
