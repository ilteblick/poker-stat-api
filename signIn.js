import speakeasy from 'speakeasy';
import { end_point_db } from './server';
import { encrypt } from './register';

export function signIn(signInModel) {
    const { email, password } = signInModel;
    let user;
    let otpauth_url;
    return end_point_db.any('select * from users where users.email = $1', [email])
        .then((result) => {
            if (result && result.length === 1) {
                user = result[0];
            } else {
                return Promise.reject(new Error('Wrong email'));
            }

            return encrypt(password, user.salt);
        })
        .then((encryptObj) => {
            const { generatedPassword } = encryptObj;
            if (generatedPassword === user.password) {
                // return Promise.resolve({ user });
                if (user.is_registered) {
                    return Promise.resolve({ user });
                }
                return Promise.reject(new Error('Check your email and confirm your account'));

                // const secret = speakeasy.generateSecret({ length: 20 });
                // otpauth_url = secret.otpauth_url;

                // return end_point_db.any('update users set auth_secret = $1 where email = $2', [secret.base32, email]);
            }
            return Promise.reject(new Error('Wrong password'));
        });
    // .then(() => end_point_db.one('select * from users where users.email = $1', [email]))
    // .then(result => Promise.resolve({
    //     user: result,
    //     otpauth_url,
    // }));
}
