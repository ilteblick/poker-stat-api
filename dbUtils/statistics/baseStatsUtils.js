export function generateQueryToPreCompiledResults(preCompiledTables, player) {
    let preCompiledQuery = '';
    for (let j = 0; j < preCompiledTables.length; j += 1) {
        const query = `select *
                                from ${preCompiledTables[j].table_name}
                                where playername = $1 and pokersite_id = $2
                                `;
        if (j + 1 !== preCompiledTables.length) {
            preCompiledQuery = `${preCompiledQuery} ${query} union`;
        } else {
            preCompiledQuery = `${preCompiledQuery} ${query}`;
        }
    }

    return {
        query: `SELECT * from (${preCompiledQuery}) as a`,
        params: [player.playername, player.pokersite_id],
    };
}

export function preparePreCompiledResults(preCompiledResults) {
    let stats = [];
    const parsedStats = preCompiledResults.map(x => JSON.parse(x.stats));

    parsedStats.forEach((x) => {
        let resultArray = [];
        if (Array.isArray) {
            resultArray = resultArray.concat(x);
        } else {
            resultArray.push(x.playedyearandmonth);
        }
        stats = stats.concat(resultArray);
    });
    const years = stats.map(x => x.playedyearandmonth);
    const uniqueYears = [...new Set(years)];

    const preparedResults = [];
    for (const y of uniqueYears) {
        const finaly = {};
        const needed = stats.filter(x => x.playedyearandmonth === y);
        if (needed) {
            for (const key in needed[0]) {
                if (!finaly[key]) {
                    finaly[key] = 0;
                }
                for (let m = 0; m < needed.length; m += 1) {
                    finaly[key] += +needed[m][key];
                }
            }
        }
        finaly.playedyearandmonth = y;
        finaly.winrate = (finaly.totalbbswon / finaly.totalhands).toFixed(1);
        preparedResults.push(finaly);
    }

    return preparedResults;
}

export function generateQueryToCompiledResults(compiledTables, conditions, player) {
    let compiledPlayerResultsQuery = '';
    if (compiledTables && compiledTables.length > 0) {
        for (let i = 0; i < compiledTables.length; i += 1) {
            const compiledplayerresultsTable = compiledTables[i];
            const tableName = compiledplayerresultsTable.table_name;
            const query = `select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
                    join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.playedyearandmonth = currencies.hm_format_date
                    where playername=$1 and pokersite_id=$2  ${conditions}
                `;
            if (i + 1 !== compiledTables.length) {
                compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query} union`;
            } else {
                compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query}`;
            }
        }
    }

    const query = `
    select playername, playedyearandmonth,
                ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
                sum(totalhands) as totalhands,
                sum(vpiphands) as vpiphands,
                sum(pfrhands) as pfrhands,
                sum(didthreebet) as didthreebet,
                sum(couldthreebet) as couldthreebet,
                sum(winnings) as winnings,
                sum(totalbbswon) as totalbbswon,
                
                sum(flopcontinuationbetmade) as flopcontinuationbetmade,
                sum(turncontinuationbetmade) as turncontinuationbetmade,
                sum(rivercontinuationbetmade) as rivercontinuationbetmade,
                sum(flopcontinuationbetpossible) as flopcontinuationbetpossible,
                sum(turncontinuationbetpossible) as turncontinuationbetpossible,
                sum(rivercontinuationbetpossible) as rivercontinuationbetpossible,

                sum(foldedtoflopcontinuationbet) as foldedtoflopcontinuationbet,
                sum(foldedtoturncontinuationbet) as foldedtoturncontinuationbet,
                sum(foldedtorivercontinuationbet) as foldedtorivercontinuationbet,
                sum(facingflopcontinuationbet) as facingflopcontinuationbet,
                sum(facingturncontinuationbet) as facingturncontinuationbet,
                sum(facingrivercontinuationbet) as facingrivercontinuationbet,

                sum(wonhandwhensawflop) as wonhandwhensawflop,
                sum(sawflop) as sawflop,
                sum(sawshowdown) as sawshowdown,
                sum(wonshowdown) as wonshowdown,



                sum(didcoldcall) as didcoldcall,
                sum(couldcoldcall) as couldcoldcall,

                sum(didsqueeze) as didsqueeze,
                sum(couldsqueeze) as couldsqueeze,

                sum(smallblindstealreraised) as smallblindstealreraised,
                sum(smallblindstealattempted) as smallblindstealattempted,

                sum(bigblindstealreraised) as bigblindstealreraised,
                sum(bigblindstealattempted) as bigblindstealattempted,

                sum(foldedtothreebetpreflop) as foldedtothreebetpreflop,
                sum(calledthreebetpreflop) as calledthreebetpreflop,
                sum(raisedthreebetpreflop) as raisedthreebetpreflop,
                sum(facedthreebetpreflop) as facedthreebetpreflop,

                sum(foldedtofourbetpreflop) as foldedtofourbetpreflop,
                sum(raisedfourbetpreflop) as raisedfourbetpreflop,
                sum(facedfourbetpreflop) as facedfourbetpreflop,

                sum(raisedflopcontinuationbet) as raisedflopcontinuationbet,
                sum(raisedturncontinuationbet) as raisedturncontinuationbet,
                sum(raisedrivercontinuationbet) as raisedrivercontinuationbet,


                sum(totalaggressivepostflopstreetsseen) as totalaggressivepostflopstreetsseen,
                sum(totalpostflopstreetsseen) as totalpostflopstreetsseen

                from(${compiledPlayerResultsQuery}) as a group by playedyearandmonth, playername
    `;

    return {
        query,
        params: [player.playername, player.pokersite_id],
    };
}

export function prepareCompiledResults(compiledResults, playername) {
    const sumResults = {
        totalhands: 0,
        vpiphands: 0,
        pfrhands: 0,
        didthreebet: 0,
        couldthreebet: 0,
        winnings: 0,
        totalbbswon: 0,
        flopcontinuationbetmade: 0,
        turncontinuationbetmade: 0,
        rivercontinuationbetmade: 0,
        flopcontinuationbetpossible: 0,
        turncontinuationbetpossible: 0,
        rivercontinuationbetpossible: 0,
        foldedtoflopcontinuationbet: 0,
        foldedtoturncontinuationbet: 0,
        foldedtorivercontinuationbet: 0,
        facingflopcontinuationbet: 0,
        facingturncontinuationbet: 0,
        facingrivercontinuationbet: 0,
        wonhandwhensawflop: 0,
        sawshowdown: 0,
        wonshowdown: 0,
        sawflop: 0,

        didcoldcall: 0,
        couldcoldcall: 0,
        didsqueeze: 0,
        couldsqueeze: 0,
        smallblindstealreraised: 0,
        smallblindstealattempted: 0,
        bigblindstealreraised: 0,
        bigblindstealattempted: 0,
        foldedtothreebetpreflop: 0,
        calledthreebetpreflop: 0,
        raisedthreebetpreflop: 0,
        facedthreebetpreflop: 0,
        foldedtofourbetpreflop: 0,
        raisedfourbetpreflop: 0,
        facedfourbetpreflop: 0,
        raisedflopcontinuationbet: 0,
        raisedturncontinuationbet: 0,
        raisedrivercontinuationbet: 0,
        totalaggressivepostflopstreetsseen: 0,
        totalpostflopstreetsseen: 0,
    };
    const monthResults = [];
    if (compiledResults && compiledResults.length > 0) {
        compiledResults.map((month) => {
            monthResults.push({
                month: month.playedyearandmonth,
                hands: month.totalhands,
                winnings: (+month.winnings).toFixed(1),
                bb100: month.winrate,
            });

            for (const key in month) {
                if (month[key] !== null && month[key] !== undefined) {
                    sumResults[key] += +month[key];
                }
            }
            return null;
        });
    }


    const stats = {
        playername,
        hands: sumResults.totalhands,
        vpip: (sumResults.vpiphands / sumResults.totalhands * 100).toFixed(1),
        pfr: (sumResults.pfrhands / sumResults.totalhands * 100).toFixed(1),
        threebet: (sumResults.didthreebet / sumResults.couldthreebet * 100).toFixed(1),
        winnings: sumResults.winnings.toFixed(1),

        winrate: (sumResults.winnings / sumResults.totalhands * 100).toFixed(1),

        bbswon: sumResults.totalbbswon,
        bb100: (sumResults.totalbbswon / sumResults.totalhands).toFixed(1),

        cbetflop: (sumResults.flopcontinuationbetmade / sumResults.flopcontinuationbetpossible * 100).toFixed(1),
        cbetturn: (sumResults.turncontinuationbetmade / sumResults.turncontinuationbetpossible * 100).toFixed(1),
        cbetriver: (sumResults.rivercontinuationbetmade / sumResults.rivercontinuationbetpossible * 100).toFixed(1),

        foldflopcbet: (sumResults.foldedtoflopcontinuationbet / sumResults.facingflopcontinuationbet * 100).toFixed(1),
        foldturncbet: (sumResults.foldedtoturncontinuationbet / sumResults.facingturncontinuationbet * 100).toFixed(1),
        foldrivercbet: (sumResults.foldedtorivercontinuationbet / sumResults.facingrivercontinuationbet * 100).toFixed(1),

        wonwhensawflop: (sumResults.wonhandwhensawflop / sumResults.sawflop * 100).toFixed(1),
        sawshowdown: (sumResults.sawshowdown / sumResults.sawflop * 100).toFixed(1),
        wonshowdown: (sumResults.wonshowdown / sumResults.sawshowdown * 100).toFixed(1),

        didcoldcall: (sumResults.didcoldcall / sumResults.couldcoldcall * 100).toFixed(1),
        didsqueeze: (sumResults.didsqueeze / sumResults.couldsqueeze * 100).toFixed(1),
        smallblindstealreraised: (sumResults.smallblindstealreraised / sumResults.smallblindstealattempted * 100).toFixed(1),
        bigblindstealreraised: (sumResults.bigblindstealreraised / sumResults.bigblindstealattempted * 100).toFixed(1),
        foldedtothreebetpreflop: (sumResults.foldedtothreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
        calledthreebetpreflop: (sumResults.calledthreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
        raisedthreebetpreflop: (sumResults.raisedthreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
        foldedtofourbetpreflop: (sumResults.foldedtofourbetpreflop / sumResults.facedfourbetpreflop * 100).toFixed(1),
        raisedfourbetpreflop: (sumResults.raisedfourbetpreflop / sumResults.facedfourbetpreflop * 100).toFixed(1),
        raisedflopcontinuationbet: (sumResults.raisedflopcontinuationbet / sumResults.facingflopcontinuationbet * 100).toFixed(1),
        raisedturncontinuationbet: (sumResults.raisedturncontinuationbet / sumResults.facingturncontinuationbet * 100).toFixed(1),
        raisedrivercontinuationbet: (sumResults.raisedrivercontinuationbet / sumResults.facingrivercontinuationbet * 100).toFixed(1),
        aggressive: (sumResults.totalaggressivepostflopstreetsseen / sumResults.totalpostflopstreetsseen * 100).toFixed(1),
    };

    return {
        stats,
        monthResults,
    };
}
