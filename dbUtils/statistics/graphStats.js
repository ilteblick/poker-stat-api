import dateFormat from 'dateformat';

import {pokerGameTypes,} from '../../server';

export function generateConditionsToGraphStats(nickname, pokerGameType, limit, tableSize, date) {
    let conditions = '';
    if (pokerGameType || limit || tableSize || date) {
        if (pokerGameType) {
            conditions = `${conditions} and pokergametype_id = ${pokerGameTypes[pokerGameType]}`;
        }

        if (limit) {
            conditions = `${conditions} and bigblindincents = ${limit}`;
        }

        if (tableSize) {
            conditions = `${conditions} and tablesize = ${tableSize}`;
        }

        switch (date) {
            case 'this_month':
                {
                    const now = new Date();
                    const formated = dateFormat(now, 'yyyymm');
                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${formated}])`;
                    break;
                }
            case 'last_month':
                {
                    const now = new Date();
                    now.setMonth(now.getMonth() - 1);
                    const formated = dateFormat(now, 'yyyymm');
                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${formated}])`;
                    break;
                }

            case 'last_three_months':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 3; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_half_year':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 6; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_twelve_months':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 12; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_year':
                {
                    const now = new Date();
                    now.setMonth(11);
                    now.setFullYear(now.getFullYear() - 1);
                    let dates = '';
                    for (let i = 0; i < 12; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                    break;
                }
            case 'this_year':
                {
                    const now = new Date();
                    const currMonth = now.getMonth();
                    now.setMonth(0);
                    let dates = '';
                    for (let i = 0; i <= currMonth; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() + 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                    break;
                }
            default:
                break;
        }
    }

    return conditions;
}

export function generateQueryToGraphStats(formatedTables, conditions, player) {
    let graphStatsQuery = '';
    if (formatedTables && formatedTables.length > 0) {
        for (let i = 0; i < formatedTables.length; i += 1) {
            const playerresultsTable = formatedTables[i];
            const tableName = playerresultsTable.table_name;
            // join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.hm_format_date = currencies.hm_format_date
            const query = `select result, hand_id from ${tableName} 
                join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.hm_format_date = currencies.hm_format_date
                    where playername=$1 and pokersite_id=$2 ${conditions}`;
            if (i + 1 !== formatedTables.length) {
                graphStatsQuery = `${graphStatsQuery} ${query} union all`;
            } else {
                graphStatsQuery = `${graphStatsQuery} ${query}`;
            }
        }
    }

    const query = `
        select result from (
            ${graphStatsQuery}
        ) as a order by hand_id
    `;

    return {
        query,
        params: [player.playername, player.pokersite_id],
    };
}

export function prepareGraphResults(graphResults) {
    const pointsCount = 50;

    if (graphResults && graphResults.length > pointsCount) {
        const dotsUnionCount = Math.floor(graphResults.length / pointsCount);

        const unionGraphResult = [];
        for (let i = 0; i < graphResults.length; i += 1) {
            const unionIndex = Math.floor(i / dotsUnionCount);
            if (!unionGraphResult[unionIndex]) {
                unionGraphResult[unionIndex] = 0;
            }

            unionGraphResult[unionIndex] += +graphResults[i].result;
        }

        return unionGraphResult;
    }
    return graphResults.map(x => x.result);
}
