import dateFormat from 'dateformat';

import {pokerGameTypes,} from '../../server';

export function generateConditionsToAgrFactor(nickname, pokerGameType, limit, tableSize, date) {
    let conditions = '';
    if (pokerGameType || limit || tableSize || date) {
        if (pokerGameType) {
            conditions = `${conditions} and pokergametype_id = ${pokerGameTypes[pokerGameType]}`;
        }

        if (limit) {
            conditions = `${conditions} and bigblindincents = ${limit}`;
        }

        if (tableSize) {
            conditions = `${conditions} and tablesize = ${tableSize}`;
        }

        switch (date) {
            case 'this_month':
                {
                    const now = new Date();
                    const formated = dateFormat(now, 'yyyymm');
                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${formated}])`;
                    break;
                }
            case 'last_month':
                {
                    const now = new Date();
                    now.setMonth(now.getMonth() - 1);
                    const formated = dateFormat(now, 'yyyymm');
                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${formated}])`;
                    break;
                }

            case 'last_three_months':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 3; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_half_year':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 6; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_twelve_months':
                {
                    const now = new Date();
                    let dates = '';
                    for (let i = 0; i < 12; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                    break;
                }
            case 'last_year':
                {
                    const now = new Date();
                    now.setMonth(11);
                    now.setFullYear(now.getFullYear() - 1);
                    let dates = '';
                    for (let i = 0; i < 12; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() - 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                    break;
                }
            case 'this_year':
                {
                    const now = new Date();
                    const currMonth = now.getMonth();
                    now.setMonth(0);
                    let dates = '';
                    for (let i = 0; i <= currMonth; i += 1) {
                        const formated = dateFormat(now, 'yyyymm');
                        now.setMonth(now.getMonth() + 1);
                        if (i !== 0) {
                            dates = `${dates},${formated}`;
                        } else {
                            dates = formated;
                        }
                    }

                    conditions = `${conditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                    break;
                }
            default:
                break;
        }
    }

    return conditions;
}

export function generateQueryToAgrFactor(additionalTables, conditions, player) {
    let additionalQuery = '';
    if (additionalTables && additionalTables.length > 0) {
        for (let i = 0; i < additionalTables.length; i += 1) {
            const additionalplayerresultsTable = additionalTables[i];
            const tableName = additionalplayerresultsTable.table_name;
            const query = `select * from ${tableName} where playername=$1 and pokersite_id=$2 ${conditions}`;
            if (i + 1 !== additionalTables.length) {
                additionalQuery = `${additionalQuery} ${query} union`;
            } else {
                additionalQuery = `${additionalQuery} ${query}`;
            }
        }
    }

    const query = `select     
    playername, pokersite_id,
    sum(flopbetmade) as flopbetmade,
    sum(flopbetpossible) as flopbetpossible,

    sum(flopraisemade) as flopraisemade,
    sum(flopraisepossible) as flopraisepossible,

    sum(flopcallmade) as flopcallmade,
    sum(flopcallpossible) as flopcallpossible,

    sum(flopcallvsbetmade) as flopcallvsbetmade,
    sum(flopraisevsbetmade) as flopraisevsbetmade,
    sum(flopfoldvsbetmade) as flopfoldvsbetmade,

    sum(turnbetmade) as turnbetmade,
    sum(turnbetpossible) as turnbetpossible,

    sum(turnraisemade) as turnraisemade,
    sum(turnraisepossible) as turnraisepossible,

    sum(turncallmade) as turncallmade, 
    sum(turncallpossible) as turncallpossible,

    sum(turncallvsbetmade) as turncallvsbetmade,
    sum(turnraisevsbetmade) as turnraisevsbetmade,
    sum(turnfoldvsbetmade) as turnfoldvsbetmade,

    sum(riverbetmade) as riverbetmade,
    sum(riverbetpossible) as riverbetpossible,

    sum(riverraisemade) as riverraisemade,
    sum(riverraisepossible) as riverraisepossible,

    sum(rivercallmade) as rivercallmade,
    sum(rivercallpossible) as rivercallpossible,

    sum(rivercallvsbetmade) as rivercallvsbetmade,
    sum(riverraisevsbetmade) as riverraisevsbetmade,
    sum(riverfoldvsbetmade) as riverfoldvsbetmade
        from (${additionalQuery}) as a group by playername, pokersite_id`;

    return {
        query,
        params: [player.playername, player.pokersite_id],
    };
}

export function generateAgrStats(stats) {
    const argObj = {
        flopbet: (stats.flopbetmade / stats.flopbetpossible * 100).toFixed(1),
        turnbet: (stats.turnbetmade / stats.turnbetpossible * 100).toFixed(1),
        riverbet: (stats.riverbetmade / stats.riverbetpossible * 100).toFixed(1),

        flopcall: (stats.flopcallmade / stats.flopcallpossible * 100).toFixed(1),
        turncall: (stats.turncallmade / stats.turncallpossible * 100).toFixed(1),
        rivercall: (stats.rivercallmade / stats.rivercallpossible * 100).toFixed(1),

        flopraise: (stats.flopraisemade / stats.flopraisepossible * 100).toFixed(1),
        turnraise: (stats.turnraisemade / stats.turnraisepossible * 100).toFixed(1),
        riverraise: (stats.riverraisemade / stats.riverraisepossible * 100).toFixed(1),
    };

    if (+stats.flopcallpossible || +stats.flopraisepossible) {
        argObj.flopfold = (100 - (argObj.flopcall || 0) - (argObj.flopraise || 0)).toFixed(1);
    } else {
        argObj.flopfold = '-';
    }

    if (+stats.turncallpossible || +stats.turnraisepossible) {
        argObj.turnfold = (100 - (argObj.turncall || 0) - (argObj.turnraise || 0)).toFixed(1);
    } else {
        argObj.turnfold = '-';
    }

    if (+stats.rivercallpossible || +stats.riverraisepossible) {
        argObj.riverfold = (100 - (argObj.rivercall || 0) - (argObj.riverraise || 0)).toFixed(1);
    } else {
        argObj.riverfold = '-';
    }

    argObj.flopagrfactor =
        (((+stats.flopbetmade || 0) + (+stats.flopraisemade || 0)) /
            ((+stats.flopbetmade || 0) + (+stats.flopraisevsbetmade || 0) + (+stats.flopcallvsbetmade || 0) +
                (+stats.flopbetpossible || 0) - (+stats.flopbetmade || 0) + (+stats.flopfoldvsbetmade || 0)) * 100).toFixed(1);

    argObj.turnagrfactor =
        (((+stats.turnbetmade || 0) + (+stats.turnraisemade || 0)) /
            ((+stats.turnbetmade || 0) + (+stats.turnraisevsbetmade || 0) + (+stats.turncallvsbetmade || 0) +
                (+stats.turnbetpossible || 0) - (+stats.turnbetmade || 0) + (+stats.turnfoldvsbetmade || 0)) * 100).toFixed(1);

    argObj.riveragrfactor =
        (((+stats.riverbetmade || 0) + (+stats.riverraisemade || 0)) /
            ((+stats.riverbetmade || 0) + (+stats.riverraisevsbetmade || 0) + (+stats.rivercallvsbetmade || 0) +
                (+stats.riverbetpossible || 0) - (+stats.riverbetmade || 0) + (+stats.riverfoldvsbetmade || 0)) * 100).toFixed(1);

    return argObj;
}
