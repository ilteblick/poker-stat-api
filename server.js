import ms from 'ms';
import cookieParser from 'cookie-parser';
import request from 'request';
import Promise from 'bluebird';
import keygen from 'keygenerator';
import requestIp from 'request-ip';
import nodemailer from 'nodemailer';
import bodyParser from 'body-parser';
import chalk from 'chalk';
import fetch from 'node-fetch';
import boolParser from 'express-query-boolean';

import {
    register,
    encrypt,
} from './register';
import { verifyToken } from './verify';
import { signIn } from './signIn';
import {
    generateJwtToken,
    verifyJwtToken,
    generateThreeRegistrationTokens,
    generateRegistrationTokenToken,
} from './jwt';
import {
    isUserHaveSearches,
    isUserCanSearchThisLimit,
} from './utils';

import StatisticsController from './controllers/statistics';

let transporter;
// nodemailer.createTestAccount(() => {
//     transporter = nodemailer.createTransport({
//         service: 'Gmail',
//         auth: {
//             user: process.env.GMAIL_LOGIN, // generated ethereal user
//             pass: process.env.GMAIL_PASS, // generated ethereal password
//         },
//     });
// });


nodemailer.createTestAccount((err, account) => {
    transporter = nodemailer.createTransport({
        service: 'Yandex',
        auth: {
            user: process.env.MAIL_LOGIN,
            pass: process.env.MAIL_PASS,
        },
    });

    // const testMailOpts = {
    //     from: 'no-reply@hand4hand.info', // sender address
    //     to: 'chuburator@mail.ru', // list of receivers
    //     subject: 'Registration', // Subject line
    //     text: 'Registration', // plain text body
    //     html: '<b>test mail</b>',
    // };

    transporter.verify((error, success) => {
        if (error) {
            console.log(error);
        } else {
            console.log(success);
        }
    });

    // transporter.sendMail(testMailOpts, (error, info) => {
    //     if (error) {
    //         console.log(error);
    //     } else {
    //         console.log(info);
    //     }
    // });
});


Promise.config({
    // Enable warnings
    warnings: true,
    // Enable long stack traces
    longStackTraces: true,
    // Enable cancellation
    cancellation: true,
    // Enable monitoring
    monitoring: true,
});

require('dotenv').config();

const express = require('express');
const cors = require('cors');
const dateFormat = require('dateformat');

export const WebAuth = {
    CookieName: 'poker-statistic-auth',
    OldCookieName: 'old-poker-statistic-auth',
    CookieOptions: {
        maxAge: ms('24h'),
        httpOnly: true,
    },
};

const pgp = require('pg-promise')({});

const CorsOptions = {
    // origin: 'http://localhost:3000',
    origin: ['http://85.93.145.2', 'http://localhost:3000', 'http://localhost:9001', 'http://hand4hand.info', 'http://www.hand4hand.info', 'http://185.186.141.90',
        'https://85.93.145.2', 'https://localhost:3000', 'https://hand4hand.info', 'https://www.hand4hand.info', 'https://185.186.141.90',
    ],
    optionsSuccessStatus: 200,
    credentials: true,
};

const end_point_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_END_POINT_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

export const pokerGameTypes = {
    nl_holdem: 0,
    nl_omaha: 1,
};

const numberOfSearches = {
    0: 100, // nl10 - nl16
    1: 100,
    2: 100,
};

export const end_point_db = pgp(end_point_cn);
const app = express();

app.use(bodyParser.json({
    limit: '50mb',
}));
app.use(cookieParser());
app.use(cors(CorsOptions));
app.use(requestIp.mw());
app.use(boolParser());

app.use((req, res, next) => {
    console.log(chalk.cyan(new Date().toString()), req.method, req.url, req.headers['user-agent']);
    next();
});

function isAuth(req, res, next) {
    const token = req.cookies[WebAuth.CookieName];
    verifyJwtToken(token)
        .then(decoded => end_point_db.one('select * from users where users.email = $1', [decoded.email]))
        .then((user) => {
            if (user.last_token !== token) {
                res.clearCookie(WebAuth.CookieName);
                return Promise.reject(new Error('No authorize. Please reload page.'));
            }
            req.user = user;
            return Promise.resolve();
        })
        .then(() => next())
        .catch(() => {
            res.statusCode = 409;
            res.json({
                message: 'No authorize. Please reload page.',
            });
        });
}

function isAdmin(req, res, next) {
    const token = req.cookies[WebAuth.CookieName];
    verifyJwtToken(token)
        .then(decoded => end_point_db.one('select * from users where users.email = $1', [decoded.email]))
        .then((user) => {
            if (user.last_token !== token || !user.is_admin) {
                res.clearCookie(WebAuth.CookieName);
                return Promise.reject(new Error('No authorize. Please reload page.'));
            }
            req.user = user;
            return Promise.resolve();
        })
        .then(() => next())
        .catch(() => {
            res.statusCode = 403;
            res.json({
                message: 'No admin access rights. Please reload page.',
            });
        });
}

const reqPrefix = '/api';

app.get(`${reqPrefix}/players`, (req, res) => {
    const {
        nickname,
    } = req.query;
    const nicknameToFind = nickname.toLowerCase();
    if (nickname) {
        end_point_db.many(`select * from players where normalized_playername like '${nicknameToFind}%' limit 5`)
            .then((result) => {
                res.json({
                    players: result,
                });
            })
            .catch(() => {
                res.statusCode = 404;
                res.json({
                    message: 'No players',
                });
            });
    } else {
        res.statusCode = 404;
        res.json({
            message: 'No players',
        });
    }
});

app.get(`${reqPrefix}/`, (req, res) => {
    res.json({
        test: 'ok',
    });
});


app.get(`${reqPrefix}/ranking`, async (req, res) => {
    const {
        pokerGameType,
        limit,
        tableSize,
        date,
        sort: sortParam,
        pokersite_id,
    } = req.query;

    const topFromCash = await end_point_db.one(`
        select * from top_players_cash where
        pokersite_id = $1 and
        bigblindincents ${limit ? `= ${limit}` : 'is null'} and
        tablesize ${tableSize ? `= ${tableSize}` : 'is null'} and
        pokergametype_id ${pokerGameType ? `= ${pokerGameType}` : 'is null'} and
        date ${date ? `= ${date}` : 'is null'} and
        sortparam = $2
    `, [pokersite_id, sortParam])
        .catch((err) => {
            console.log(err);
        });

    if (topFromCash) {
        res.json({
            topPlayers: JSON.parse(topFromCash.stats),
        });
    } else {
        const compiledTables = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');

        let condition = '';
        if (pokerGameType || limit || tableSize || date) {
            if (pokerGameType) {
                condition = `${condition} and pokergametype_id = ${pokerGameTypes[pokerGameType]}`;
            }

            if (limit) {
                condition = `${condition} and bigblindincents = ${limit}`;
            }

            if (tableSize) {
                condition = `${condition} and tablesize = ${tableSize}`;
            }

            switch (date) {
                case 'this_month':
                    {
                        const now = new Date();
                        const formated = dateFormat(now, 'yyyymm');
                        condition = `${condition} and playedyearandmonth = any(ARRAY[${formated}])`;
                        break;
                    }
                case 'last_month':
                    {
                        const now = new Date();
                        now.setMonth(now.getMonth() - 1);
                        const formated = dateFormat(now, 'yyyymm');
                        condition = `${condition} and playedyearandmonth = any(ARRAY[${formated}])`;
                        break;
                    }

                case 'last_three_months':
                    {
                        const now = new Date();
                        let dates = '';
                        for (let i = 0; i < 3; i += 1) {
                            const formated = dateFormat(now, 'yyyymm');
                            now.setMonth(now.getMonth() - 1);
                            if (i !== 0) {
                                dates = `${dates},${formated}`;
                            } else {
                                dates = formated;
                            }
                        }

                        condition = `${condition} and playedyearandmonth = any(ARRAY[${dates}])`;
                        break;
                    }
                case 'last_half_year':
                    {
                        const now = new Date();
                        let dates = '';
                        for (let i = 0; i < 6; i += 1) {
                            const formated = dateFormat(now, 'yyyymm');
                            now.setMonth(now.getMonth() - 1);
                            if (i !== 0) {
                                dates = `${dates},${formated}`;
                            } else {
                                dates = formated;
                            }
                        }

                        condition = `${condition} and playedyearandmonth = any(ARRAY[${dates}])`;
                        break;
                    }
                case 'last_twelve_months':
                    {
                        const now = new Date();
                        let dates = '';
                        for (let i = 0; i < 12; i += 1) {
                            const formated = dateFormat(now, 'yyyymm');
                            now.setMonth(now.getMonth() - 1);
                            if (i !== 0) {
                                dates = `${dates},${formated}`;
                            } else {
                                dates = formated;
                            }
                        }

                        condition = `${condition} and playedyearandmonth = any(ARRAY[${dates}])`;
                        break;
                    }
                case 'last_year':
                    {
                        const now = new Date();
                        now.setMonth(11);
                        now.setFullYear(now.getFullYear() - 1);
                        let dates = '';
                        for (let i = 0; i < 12; i += 1) {
                            const formated = dateFormat(now, 'yyyymm');
                            now.setMonth(now.getMonth() - 1);
                            if (i !== 0) {
                                dates = `${dates},${formated}`;
                            } else {
                                dates = formated;
                            }
                        }

                        condition = `${condition} and playedyearandmonth = any(ARRAY[${dates}])`;
                        break;
                    }
                case 'this_year':
                    {
                        const now = new Date();
                        const currMonth = now.getMonth();
                        now.setMonth(0);
                        let dates = '';
                        for (let i = 0; i <= currMonth; i += 1) {
                            const formated = dateFormat(now, 'yyyymm');
                            now.setMonth(now.getMonth() + 1);
                            if (i !== 0) {
                                dates = `${dates},${formated}`;
                            } else {
                                dates = formated;
                            }
                        }

                        condition = `${condition} and playedyearandmonth = any(ARRAY[${dates}])`;
                        break;
                    }

                default:
                    break;
            }
        }
        let compiledPlayerResultsQuery = '';
        if (compiledTables && compiledTables.length > 0) {
            for (let i = 0; i < compiledTables.length; i += 1) {
                const compiledplayerresultsTable = compiledTables[i];
                const tableName = compiledplayerresultsTable.table_name;
                const query = `select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
                            join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.playedyearandmonth = currencies.hm_format_date
                            where pokersite_id=$1  ${condition}
                        `;
                if (i + 1 !== compiledTables.length) {
                    compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query} union`;
                } else {
                    compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query}`;
                }
            }
        }

        const topPlayers = await end_point_db.many(`
                select a.playername,

                ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
                sum(totalhands) as totalhands,
    
                sum(winnings) as winnings,
                home_limit        

                from(${compiledPlayerResultsQuery}) as a 
                join players on players.playername = a.playername and players.pokersite_id = a.pokersite_id
                group by a.playername, players.home_limit
                order by ${sortParam} limit 10`, [pokersite_id])
            .catch((err) => {
                res.statusCode = 405;
                res.json({
                    message: 'There is no data for these filters',
                });
            });

        if (topPlayers) {
            res.json({
                topPlayers,
            });
        }
    }
});

app.get(`${reqPrefix}/playersToChoose`, isAuth, async (req, res) => {
    const {
        nickname,
    } = req.query;


    const players = await end_point_db.many('select * from players where normalized_playername = $1', [nickname.toLowerCase()])
        .catch(() => {
            res.statusCode = 404;
            res.json({
                message: 'No players with such nickname',
            });
        });

    if (players) {
        res.json({
            players,
        });
    }
});

app.get(`${reqPrefix}/check-permissions`, isAuth, async (req, res) => {
    StatisticsController.checkPermissions(req, res);
});

app.get(`${reqPrefix}/statistics/agr-factor`, isAuth, async (req, res) => {
    StatisticsController.getAgrFactorStats(req, res);
});

app.get(`${reqPrefix}/statistics/base-stats`, async (req, res) => {
    StatisticsController.getBaseStats(req, res);
});

app.get(`${reqPrefix}/statistics/graph-stats`, isAuth, async (req, res) => {
    StatisticsController.getGraphStats(req, res);
});

app.get(`${reqPrefix}/stats`, isAuth, async (req, res) => {
    const {
        nickname,
        pokerGameType,
        limit,
        tableSize,
        date,
        pokersite_id,
    } = req.query;

    console.log(chalk.cyan(new Date().toString()), 'get statistics', nickname, pokerGameType, limit, tableSize, date);


    const nicknameToFind = nickname.toLowerCase();

    let mainStatsResult;
    let compiledplayerresultsTables;
    let playerresultsTables;
    let playerResultsQuery = '';
    const user = req.user;
    const monthResults = [];

    let start = new Date();
    let end;
    let cash;

    let player;


    // try {    //     if (!user.is_admin) {
    //         if (!isUserHaveSearches(user.number_of_searches)) {
    //             const err = new Error('You havent got any searches now.');
    //             err.status = 403;
    //             throw (err);
    //         }
    //         if (!isUserCanSearchThisLimit(limit, user.subscription_type)) {
    //             const err = new Error('You cant search this limit.');
    //             err.status = 403;
    //             throw (err);
    //         }
    //     }

    //     await end_point_db.any('update users set number_of_searches = number_of_searches - 1 where email = $1', [user.email]);
    //     const player = await end_point_db.one('select * from players where normalized_playername = $1', [nickname.toLowerCase()]);

    //     let cash;
    //     if (!pokerGameType && !tableSize && !limit && !date) {
    //         cash = await end_point_db.one(
    //             'select * from cash where playername=$1 and pokersite_id=$2',
    //             [player.playername, player.pokersite_id],
    //         ).catch(() => {

    //         });
    //     }

    //     if (cash) {
    //         res.json(JSON.parse(cash.stats));
    //     } else {
    //         const compiledplayerresultsTables =
    //             await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');
    //         const playerresultsTables =
    //             end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'');
    //     }
    // } catch (err) {

    // }

    const p = Promise.resolve()
        .then(() => end_point_db.one(
            'select * from players where normalized_playername = $1 and pokersite_id = $2',
            [nickname.toLowerCase(), pokersite_id],
        ))
        .then((pl) => {
            player = pl;
            return new Promise((resolve, reject) => {
                if (!user.is_admin) {
                    if (!user.is_unlimit && !isUserHaveSearches(user.number_of_searches)) {
                        res.statusCode = 409;
                        res.json({
                            message: 'Your limit is over for today.',
                        });
                        return p.cancel();
                    }

                    if (!isUserCanSearchThisLimit(limit, user.subscription_type, player.home_limit)) {
                        res.statusCode = 409;
                        res.json({
                            message: 'You cant search this limit.',
                        });
                        return p.cancel();
                    }
                }
                resolve();
            });
        })
        .then(() => end_point_db.any('update users set number_of_searches = number_of_searches - 1 where email = $1', [user.email]))
        .then(() => end_point_db.one('select * from cash where playername=$1 and pokersite_id=$2', [player.playername, player.pokersite_id]))
        .then((c) => {
            cash = c;
            if (!pokerGameType && !tableSize && !limit && !date) {
                res.json(JSON.parse(c.stats));
                return p.cancel();
            }
            return p.resolve();
        })
        .catch(() => end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\''))
        .then((tableNames) => {
            compiledplayerresultsTables = tableNames;
            return end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'');
        })
        .then((tableNames) => {
            playerresultsTables = tableNames;
            let firstConditions = '';
            let secondConditions = '';
            if (pokerGameType || limit || tableSize || date) {
                if (pokerGameType) {
                    firstConditions = `${firstConditions} and pokergametype_id = ${pokerGameTypes[pokerGameType]}`;
                }

                if (limit) {
                    firstConditions = `${firstConditions} and bigblindincents = ${limit}`;
                }

                if (tableSize) {
                    firstConditions = `${firstConditions} and tablesize = ${tableSize}`;
                }

                secondConditions = firstConditions;
                switch (date) {
                    case 'this_month':
                        {
                            const now = new Date();
                            const formated = dateFormat(now, 'yyyymm');
                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${formated}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${formated}])`;
                            break;
                        }
                    case 'last_month':
                        {
                            const now = new Date();
                            now.setMonth(now.getMonth() - 1);
                            const formated = dateFormat(now, 'yyyymm');
                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${formated}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${formated}])`;
                            break;
                        }

                    case 'last_three_months':
                        {
                            const now = new Date();
                            let dates = '';
                            for (let i = 0; i < 3; i += 1) {
                                const formated = dateFormat(now, 'yyyymm');
                                now.setMonth(now.getMonth() - 1);
                                if (i !== 0) {
                                    dates = `${dates},${formated}`;
                                } else {
                                    dates = formated;
                                }
                            }

                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                            break;
                        }
                    case 'last_half_year':
                        {
                            const now = new Date();
                            let dates = '';
                            for (let i = 0; i < 6; i += 1) {
                                const formated = dateFormat(now, 'yyyymm');
                                now.setMonth(now.getMonth() - 1);
                                if (i !== 0) {
                                    dates = `${dates},${formated}`;
                                } else {
                                    dates = formated;
                                }
                            }

                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                            break;
                        }
                    case 'last_twelve_months':
                        {
                            const now = new Date();
                            let dates = '';
                            for (let i = 0; i < 12; i += 1) {
                                const formated = dateFormat(now, 'yyyymm');
                                now.setMonth(now.getMonth() - 1);
                                if (i !== 0) {
                                    dates = `${dates},${formated}`;
                                } else {
                                    dates = formated;
                                }
                            }

                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                            break;
                        }
                    case 'last_year':
                        {
                            const now = new Date();
                            now.setMonth(11);
                            now.setFullYear(now.getFullYear() - 1);
                            let dates = '';
                            for (let i = 0; i < 12; i += 1) {
                                const formated = dateFormat(now, 'yyyymm');
                                now.setMonth(now.getMonth() - 1);
                                if (i !== 0) {
                                    dates = `${dates},${formated}`;
                                } else {
                                    dates = formated;
                                }
                            }

                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                            break;
                        }
                    case 'this_year':
                        {
                            const now = new Date();
                            const currMonth = now.getMonth();
                            now.setMonth(0);
                            let dates = '';
                            for (let i = 0; i <= currMonth; i += 1) {
                                const formated = dateFormat(now, 'yyyymm');
                                now.setMonth(now.getMonth() + 1);
                                if (i !== 0) {
                                    dates = `${dates},${formated}`;
                                } else {
                                    dates = formated;
                                }
                            }

                            firstConditions = `${firstConditions} and playedyearandmonth = any(ARRAY[${dates}])`;
                            secondConditions = `${secondConditions} and currencies.hm_format_date = any(ARRAY[${dates}])`;
                            break;
                        }

                    default:
                        break;
                }
            }

            let compiledPlayerResultsQuery = '';
            if (compiledplayerresultsTables && compiledplayerresultsTables.length > 0) {
                for (let i = 0; i < compiledplayerresultsTables.length; i += 1) {
                    const compiledplayerresultsTable = compiledplayerresultsTables[i];
                    const tableName = compiledplayerresultsTable.table_name;
                    const query = `select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
                            join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.playedyearandmonth = currencies.hm_format_date
                            where normalized_playername=$1 and pokersite_id=$2  ${firstConditions}
                        `;
                    if (i + 1 !== compiledplayerresultsTables.length) {
                        compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query} union`;
                    } else {
                        compiledPlayerResultsQuery = `${compiledPlayerResultsQuery} ${query}`;
                    }
                }
            }


            if (playerresultsTables && playerresultsTables.length > 0) {
                for (let i = 0; i < playerresultsTables.length; i += 1) {
                    const playerresultsTable = playerresultsTables[i];
                    const tableName = playerresultsTable.table_name;
                    // join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.hm_format_date = currencies.hm_format_date
                    const query = `select result, hand_id from ${tableName} 
                        join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.hm_format_date = currencies.hm_format_date
                            where normalized_playername=$1 and pokersite_id=$2 ${secondConditions}`;
                    if (i + 1 !== playerresultsTables.length) {
                        playerResultsQuery = `${playerResultsQuery} ${query} union all`;
                    } else {
                        playerResultsQuery = `${playerResultsQuery} ${query}`;
                    }
                }
            }

            let preCompiledTables;
            if (!pokerGameType && !tableSize && !limit && !date) {
                return end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'pre_mycompiledplayerresults%\'')
                    .then((preCompiled) => {
                        preCompiledTables = preCompiled;
                        let preCompiledQuery = '';
                        for (let j = 0; j < preCompiledTables.length; j += 1) {
                            const query = `select *
                                from ${preCompiledTables[j].table_name}
                                where playername = $1 and pokersite_id = $2
                                `;
                            if (j + 1 !== preCompiledTables.length) {
                                preCompiledQuery = `${preCompiledQuery} ${query} union`;
                            } else {
                                preCompiledQuery = `${preCompiledQuery} ${query}`;
                            }
                        }

                        return end_point_db.many(`SELECT * from (${preCompiledQuery})as a`, [player.playername, player.pokersite_id]);
                    })
                    .then((results) => {
                        let stats = [];
                        const tmp = results.map(x => JSON.parse(x.stats));


                        tmp.map((x) => {
                            let arr = [];
                            if (Array.isArray) {
                                arr = arr.concat(x);
                            } else {
                                arr.push(x.playedyearandmonth);
                            }
                            stats = stats.concat(arr);
                        });
                        const years = stats.map(x => x.playedyearandmonth);
                        const uniqueYears = [...new Set(years)];

                        const r = [];
                        for (const y of uniqueYears) {
                            const finaly = {};
                            const needed = stats.filter(x => x.playedyearandmonth === y);
                            if (needed) {
                                for (const key in needed[0]) {
                                    if (!finaly[key]) {
                                        finaly[key] = 0;
                                    }
                                    for (let m = 0; m < needed.length; m += 1) {
                                        finaly[key] += +needed[m][key];
                                    }
                                }
                            }
                            finaly.playedyearandmonth = y;
                            finaly.winrate = (finaly.totalbbswon / finaly.totalhands).toFixed(1);
                            r.push(finaly);
                        }


                        return r;
                    });
            }

            // const a = pgp.as.format(`
            // select playername, playedyearandmonth,
            // ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
            // sum(totalhands) as totalhands,
            // sum(vpiphands) as vpiphands,
            // sum(pfrhands) as pfrhands,
            // sum(didthreebet) as didthreebet,
            // sum(couldthreebet) as couldthreebet,
            // sum(winnings) as winnings,
            // sum(totalbbswon) as totalbbswon,

            // sum(flopcontinuationbetmade) as flopcontinuationbetmade,
            // sum(turncontinuationbetmade) as turncontinuationbetmade,
            // sum(rivercontinuationbetmade) as rivercontinuationbetmade,
            // sum(flopcontinuationbetpossible) as flopcontinuationbetpossible,
            // sum(turncontinuationbetpossible) as turncontinuationbetpossible,
            // sum(rivercontinuationbetpossible) as rivercontinuationbetpossible,

            // sum(foldedtoflopcontinuationbet) as foldedtoflopcontinuationbet,
            // sum(foldedtoturncontinuationbet) as foldedtoturncontinuationbet,
            // sum(foldedtorivercontinuationbet) as foldedtorivercontinuationbet,
            // sum(facingflopcontinuationbet) as facingflopcontinuationbet,
            // sum(facingturncontinuationbet) as facingturncontinuationbet,
            // sum(facingrivercontinuationbet) as facingrivercontinuationbet,

            // sum(wonhandwhensawflop) as wonhandwhensawflop,
            // sum(sawflop) as sawflop,
            // sum(sawshowdown) as sawshowdown,
            // sum(wonshowdown) as wonshowdown
            // from(${compiledPlayerResultsQuery}) as a group by playedyearandmonth, playername`, [nicknameToFind]);

            return end_point_db.many(`
                select playername, playedyearandmonth,
                ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
                sum(totalhands) as totalhands,
                sum(vpiphands) as vpiphands,
                sum(pfrhands) as pfrhands,
                sum(didthreebet) as didthreebet,
                sum(couldthreebet) as couldthreebet,
                sum(winnings) as winnings,
                sum(totalbbswon) as totalbbswon,
                
                sum(flopcontinuationbetmade) as flopcontinuationbetmade,
                sum(turncontinuationbetmade) as turncontinuationbetmade,
                sum(rivercontinuationbetmade) as rivercontinuationbetmade,
                sum(flopcontinuationbetpossible) as flopcontinuationbetpossible,
                sum(turncontinuationbetpossible) as turncontinuationbetpossible,
                sum(rivercontinuationbetpossible) as rivercontinuationbetpossible,

                sum(foldedtoflopcontinuationbet) as foldedtoflopcontinuationbet,
                sum(foldedtoturncontinuationbet) as foldedtoturncontinuationbet,
                sum(foldedtorivercontinuationbet) as foldedtorivercontinuationbet,
                sum(facingflopcontinuationbet) as facingflopcontinuationbet,
                sum(facingturncontinuationbet) as facingturncontinuationbet,
                sum(facingrivercontinuationbet) as facingrivercontinuationbet,

                sum(wonhandwhensawflop) as wonhandwhensawflop,
                sum(sawflop) as sawflop,
                sum(sawshowdown) as sawshowdown,
                sum(wonshowdown) as wonshowdown,



                sum(didcoldcall) as didcoldcall,
                sum(couldcoldcall) as couldcoldcall,

                sum(didsqueeze) as didsqueeze,
                sum(couldsqueeze) as couldsqueeze,

                sum(smallblindstealreraised) as smallblindstealreraised,
                sum(smallblindstealattempted) as smallblindstealattempted,

                sum(bigblindstealreraised) as bigblindstealreraised,
                sum(bigblindstealattempted) as bigblindstealattempted,

                sum(foldedtothreebetpreflop) as foldedtothreebetpreflop,
                sum(calledthreebetpreflop) as calledthreebetpreflop,
                sum(raisedthreebetpreflop) as raisedthreebetpreflop,
                sum(facedthreebetpreflop) as facedthreebetpreflop,

                sum(foldedtofourbetpreflop) as foldedtofourbetpreflop,
                sum(raisedfourbetpreflop) as raisedfourbetpreflop,
                sum(facedfourbetpreflop) as facedfourbetpreflop,

                sum(raisedflopcontinuationbet) as raisedflopcontinuationbet,
                sum(raisedturncontinuationbet) as raisedturncontinuationbet,
                sum(raisedrivercontinuationbet) as raisedrivercontinuationbet,


                sum(totalaggressivepostflopstreetsseen) as totalaggressivepostflopstreetsseen,
                sum(totalpostflopstreetsseen) as totalpostflopstreetsseen

                from(${compiledPlayerResultsQuery}) as a group by playedyearandmonth, playername`, [nicknameToFind, player.pokersite_id]);
        })
        .catch(() => Promise.reject(new Error('No results for this request.')))
        .then((result) => {
            const sumResults = {
                totalhands: 0,
                vpiphands: 0,
                pfrhands: 0,
                didthreebet: 0,
                couldthreebet: 0,
                winnings: 0,
                totalbbswon: 0,
                flopcontinuationbetmade: 0,
                turncontinuationbetmade: 0,
                rivercontinuationbetmade: 0,
                flopcontinuationbetpossible: 0,
                turncontinuationbetpossible: 0,
                rivercontinuationbetpossible: 0,
                foldedtoflopcontinuationbet: 0,
                foldedtoturncontinuationbet: 0,
                foldedtorivercontinuationbet: 0,
                facingflopcontinuationbet: 0,
                facingturncontinuationbet: 0,
                facingrivercontinuationbet: 0,
                wonhandwhensawflop: 0,
                sawshowdown: 0,
                wonshowdown: 0,
                sawflop: 0,

                didcoldcall: 0,
                couldcoldcall: 0,
                didsqueeze: 0,
                couldsqueeze: 0,
                smallblindstealreraised: 0,
                smallblindstealattempted: 0,
                bigblindstealreraised: 0,
                bigblindstealattempted: 0,
                foldedtothreebetpreflop: 0,
                calledthreebetpreflop: 0,
                raisedthreebetpreflop: 0,
                facedthreebetpreflop: 0,
                foldedtofourbetpreflop: 0,
                raisedfourbetpreflop: 0,
                facedfourbetpreflop: 0,
                raisedflopcontinuationbet: 0,
                raisedturncontinuationbet: 0,
                raisedrivercontinuationbet: 0,
                totalaggressivepostflopstreetsseen: 0,
                totalpostflopstreetsseen: 0,
            };
            if (result && result.length > 0) {
                result.map((month) => {
                    monthResults.push({
                        month: month.playedyearandmonth,
                        hands: month.totalhands,
                        winnings: (+month.winnings).toFixed(1),
                        bb100: month.winrate,
                    });

                    for (const key in month) {
                        if (month[key] !== null && month[key] !== undefined) {
                            sumResults[key] += +month[key];
                        }
                    }
                    return null;
                });
            }


            const resultObj = {
                playername: nickname,
                hands: sumResults.totalhands,
                vpip: (sumResults.vpiphands / sumResults.totalhands * 100).toFixed(1),
                pfr: (sumResults.pfrhands / sumResults.totalhands * 100).toFixed(1),
                threebet: (sumResults.didthreebet / sumResults.couldthreebet * 100).toFixed(1),
                winnings: sumResults.winnings.toFixed(1),

                winrate: (sumResults.winnings / sumResults.totalhands * 100).toFixed(1),

                bbswon: sumResults.totalbbswon,
                bb100: (sumResults.totalbbswon / sumResults.totalhands).toFixed(1),

                cbetflop: (sumResults.flopcontinuationbetmade / sumResults.flopcontinuationbetpossible * 100).toFixed(1),
                cbetturn: (sumResults.turncontinuationbetmade / sumResults.turncontinuationbetpossible * 100).toFixed(1),
                cbetriver: (sumResults.rivercontinuationbetmade / sumResults.rivercontinuationbetpossible * 100).toFixed(1),

                foldflopcbet: (sumResults.foldedtoflopcontinuationbet / sumResults.facingflopcontinuationbet * 100).toFixed(1),
                foldturncbet: (sumResults.foldedtoturncontinuationbet / sumResults.facingturncontinuationbet * 100).toFixed(1),
                foldrivercbet: (sumResults.foldedtorivercontinuationbet / sumResults.facingrivercontinuationbet * 100).toFixed(1),

                wonwhensawflop: (sumResults.wonhandwhensawflop / sumResults.sawflop * 100).toFixed(1),
                sawshowdown: (sumResults.sawshowdown / sumResults.sawflop * 100).toFixed(1),
                wonshowdown: (sumResults.wonshowdown / sumResults.sawshowdown * 100).toFixed(1),

                didcoldcall: (sumResults.didcoldcall / sumResults.couldcoldcall * 100).toFixed(1),
                didsqueeze: (sumResults.didsqueeze / sumResults.couldsqueeze * 100).toFixed(1),
                smallblindstealreraised: (sumResults.smallblindstealreraised / sumResults.smallblindstealattempted * 100).toFixed(1),
                bigblindstealreraised: (sumResults.bigblindstealreraised / sumResults.bigblindstealattempted * 100).toFixed(1),
                foldedtothreebetpreflop: (sumResults.foldedtothreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
                calledthreebetpreflop: (sumResults.calledthreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
                raisedthreebetpreflop: (sumResults.raisedthreebetpreflop / sumResults.facedthreebetpreflop * 100).toFixed(1),
                foldedtofourbetpreflop: (sumResults.foldedtofourbetpreflop / sumResults.facedfourbetpreflop * 100).toFixed(1),
                raisedfourbetpreflop: (sumResults.raisedfourbetpreflop / sumResults.facedfourbetpreflop * 100).toFixed(1),
                raisedflopcontinuationbet: (sumResults.raisedflopcontinuationbet / sumResults.facingflopcontinuationbet * 100).toFixed(1),
                raisedturncontinuationbet: (sumResults.raisedturncontinuationbet / sumResults.facingturncontinuationbet * 100).toFixed(1),
                raisedrivercontinuationbet: (sumResults.raisedrivercontinuationbet / sumResults.facingrivercontinuationbet * 100).toFixed(1),
                aggressive: (sumResults.totalaggressivepostflopstreetsseen / sumResults.totalpostflopstreetsseen * 100).toFixed(1),
            };

            end = new Date();

            console.log(chalk.red(`get stats:${(start.getTime() - end.getTime()) / 1000}sec`));
            if (resultObj.hands === 0) {
                return Promise.reject(new Error('No results for this request.'));
            }
            mainStatsResult = resultObj;
            const count = parseInt(resultObj.hands) / 100;
            let blockSize = Math.floor(count / 50);
            if (blockSize === 0) {
                blockSize = 1;
            }
            start = new Date();
            // if (!pokerGameType && !tableSize && !limit && !date) {
            //     let preFormated;
            //     return end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'pre_formatedplayerresults%\'')
            //         .then((preFormatedFromDb) => {
            //             preFormated = preFormatedFromDb;
            //             let queryToDb = '';
            //             for (let j = 0; j < preFormated.length; j += 1) {
            //                 const query = `select num, hm_format_date, result
            //                     from ${preFormated[j].table_name}
            //                     where playername = $1 and pokersite_id = $2
            //                 `;
            //                 if (j + 1 !== preFormated.length) {
            //                     queryToDb = `${queryToDb} ${query} union`;
            //                 } else {
            //                     queryToDb = `${queryToDb} ${query}`;
            //                 }
            //             }

            //             // const a = pgp.as.format(`select count(*) from (
            //             //     ${queryToDb}
            //             // )as a`, [player.playername, player.pokersite_id]);

            //             return end_point_db.one(`
            //                 select count(*) from (
            //                     ${queryToDb}
            //                 )as a`, [player.playername, player.pokersite_id])
            //                 .then(({ count: size }) => {
            //                     const block = +size / 50;
            //                     return end_point_db.any(
            //                         `select cast (rnum / $3 as int) as num, sum(result) from (
            //                         select row_number() OVER () as rnum, result from (
            //                         select result  from (
            //                             ${queryToDb}
            //                     ) as a order by hm_format_date, num) as b) as c group by num`
            //                         , [player.playername, player.pokersite_id, block],
            //                     );
            //                 })
            //                 .catch((err) => {
            //                     console.log(err);
            //                 });
            //         });


            //     // return end_point_db.one('select * from players where normalized_playername = $1', [nickname.toLowerCase()])
            //     //     .then((player) => {
            //     //         console.log(player);
            //     //         return end_point_db.one('select * from normalized_player_results where player_id = $1', [player.id]);
            //     //     })
            //     //     .then((graph) => {
            //     //         console.log();
            //     //         return graph.graph.split(',').map((x, index) => ({ num: index, sum: x }));
            //     //     });
            // }

            const a = pgp.as.format(`${playerResultsQuery}`, [nicknameToFind, blockSize]);

            return end_point_db.any(`
            select cast (rnum / $3 as int) as num, sum(result) from (
                select row_number() OVER () as rnum, result from (
                select result from (
                    ${playerResultsQuery}
                ) as a order by hand_id) as b) as c group by num`, [nicknameToFind, player.pokersite_id, blockSize]);
        })
        .then((graphResult) => {
            end = new Date();
            console.log(chalk.red(`get graph:${(start.getTime() - end.getTime()) / 1000}sec`));
            monthResults.sort((a, b) => a.month < b.month);

            if (graphResult && graphResult.length === 0) {
                let queryRes = '';
                for (let i = 0; i < playerresultsTables.length; i += 1) {
                    const playerresultsTable = playerresultsTables[i];
                    const tableName = playerresultsTable.table_name;
                    const query = `select * from ${tableName} 
                            join currencies on ${tableName}.currencytype_id = currencies.currency_id and ${tableName}.hm_format_date = currencies.hm_format_date
                            where normalized_playername=$1 and pokersite_id=$2`;
                    if (i + 1 !== playerresultsTables.length) {
                        queryRes = `${queryRes} ${query} union all`;
                    } else {
                        queryRes = `${queryRes} ${query}`;
                    }
                }
                const count = parseInt(mainStatsResult.hands) / 100;
                let blockSize = Math.floor(count / 50);
                if (blockSize === 0) {
                    blockSize = 1;
                }
                return end_point_db.any(`
                    select cast (rnum / $3 as int) as num, sum(result) from (
                    select row_number() OVER () as rnum, result from (
                    select result from (
                        ${queryRes}
                    ) as a order by hand_id) as b) as c group by num`, [nicknameToFind, player.pokersite_id, blockSize])
                    .then((fakeGraph) => {
                        if (!pokerGameType && !tableSize && !limit && !date) {
                            end_point_db.any(
                                'insert into cash (playername, pokersite_id, stats) VALUES ($1, $2, $3)',
                                [player.playername, player.pokersite_id, JSON.stringify({
                                    mainStatsResult,
                                    graphResult: fakeGraph,
                                    monthResults,
                                    home: player.home_limit,
                                })],
                            );
                        }

                        res.json({
                            mainStatsResult,
                            graphResult: fakeGraph,
                            monthResults,
                            home: player.home_limit,
                        });
                    });
            }

            let s = 0;
            for (const q of graphResult) {
                s += +q.sum;
            }

            const dif = (mainStatsResult.winnings - s) / graphResult.length;
            for (const q of graphResult) {
                q.sum += dif;
            }

            s = 0;
            for (const q of graphResult) {
                s += +q.sum;
            }

            if (!pokerGameType && !tableSize && !limit && !date) {
                end_point_db.any(
                    'insert into cash (playername, pokersite_id, stats) VALUES ($1, $2, $3)',
                    [player.playername, player.pokersite_id, JSON.stringify({
                        mainStatsResult,
                        graphResult,
                        monthResults,
                        home: player.home_limit,
                    })],
                );
            }


            res.json({
                mainStatsResult,
                graphResult,
                monthResults,
                home: player.home_limit,
            });
        })
        .catch((error) => {
            res.statusCode = 409;
            res.json({
                message: error.message,
            });
        });
});

app.post(`${reqPrefix}/register`, (req, res) => {
    let registerResult;
    register(req.body)
        .then((result) => {
            registerResult = result;
            return generateThreeRegistrationTokens(result.user.email);
        })
        .then(tokens => end_point_db.any(
            `insert into registration_tokens (token, status, user_id)
                 VALUES 
                 ($2, 'free', $1),
                 ($3, 'free', $1),
                 ($4, 'free', $1)`,
            [registerResult.user.id, tokens[0], tokens[1], tokens[2]],
        ))
        .then(() => {
            const mailOptions = {
                from: process.env.MAIL_LOGIN, // sender address
                to: `${registerResult.user.email}`, // list of receivers
                subject: 'Registration', // Subject line
                text: 'Registration', // plain text body
                html: `<b><a href="http://hand4hand.info/confirm?token=${registerResult.user.confirmation_token}&email=${registerResult.user.email}">Click here to complete registration</a></b>`, // html body
                // html: `<a href="https://hand4hand.info/confirm?token=${registerResult.user.confirmation_token}&email=${registerResult.user.email}/">here</a>`, // html body
                // html: `<b><a href="https://hand4hand.info/confirm?token=${registerResult.user.confirmation_token}&email=${registerResult.user.email}/>click</a></b>`,
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return Promise.reject(new Error('womething wrong'));
                }
                res.json({
                    status: 'OK',
                });
            });
        })
        .catch((error) => {
            res.statusCode = 409;
            res.json({
                message: error.message,
            });
        });
});

app.post(`${reqPrefix}/confirm`, (req, res) => {
    const {
        token,
        email,
    } = req.body;

    end_point_db.one('select * from users where email=$1 and confirmation_token=$2 and is_registered=$3', [email, token, false])
        .then(() => end_point_db.any('update users set is_registered = $1 where email = $2', [true, email]))
        .then(() => {
            res.json({
                ok: true,
            });
        })
        .catch(() => {
            res.statusCode = 405;
            res.json({
                message: 'wrong confirmation token',
            });
        });
});

app.post(`${reqPrefix}/verify_token`, (req, res) => {
    const {
        email,
        token,
    } = req.body;
    let userToResp;
    let generatedToken;
    verifyToken(email, token)
        .then((user) => {
            if (user.is_registered) {
                return Promise.resolve(user);
            }
            return end_point_db.any('update users set is_registered = $1 where email = $2', [true, email]);
        })
        .then((user) => {
            if (user && user.length !== 0) {
                return Promise.resolve(user);
            }
            return end_point_db.one('select * from users where users.email = $1', [email]);
        })
        .then((user) => {
            userToResp = user;
            return generateJwtToken(user);
        })
        .then((jwtToken) => {
            generatedToken = jwtToken;
            return end_point_db.any('update users set last_token = $1 where email = $2', [generatedToken, email]);
        })
        .then(() => {
            res.clearCookie(WebAuth.CookieName);
            res.cookie(WebAuth.CookieName, generatedToken, {
                expire: new Date() + 60 * 10000000,
            });
            res.json({
                id: userToResp.id,
                email: userToResp.email,
                is_registered: userToResp.is_registered,
                is_admin: userToResp.is_admin,
                subscription_type: userToResp.subscription_type,
                cookie: generatedToken,
            });
        })
        .catch((error) => {
            res.statusCode = 409;
            res.json({
                message: error.message,
            });
        });
});

app.post(`${reqPrefix}/sign_in`, (req, res) => {
    const loginData = req.body;
    let userToResp;
    let generatedToken;

    if (loginData && loginData.email && loginData.password) {
        signIn(req.body)
            .then((result) => {
                // const { otpauth_url, user } = result;
                // const response = {
                //     user: {
                //         id: user.id,
                //         email: user.email,
                //         is_registered: user.is_registered,
                //     },
                //     otpauth_url,
                // };
                // res.json(response);
                const {
                    user,
                } = result;
                userToResp = user;
                return generateJwtToken(user);
            })
            .then((jwtToken) => {
                generatedToken = jwtToken;
                if (userToResp.last_token !== null) {
                    const {
                        auth,
                        oldAuth,
                    } = req.body;
                    if (req.cookies[WebAuth.CookieName] === userToResp.last_token ||
                        req.cookies[WebAuth.OldCookieName] === userToResp.last_token ||
                        auth === userToResp.last_token ||
                        oldAuth === userToResp.last_token) {
                        if (userToResp.expiration_timeout_date === null) {
                            return end_point_db.any('update users set last_token = $1 where email = $2', [generatedToken, userToResp.email]);
                        }
                        return new Promise((resolve, reject) => {
                            request.get({
                                url: 'http://localhost:3002',
                                json: true,
                            }, (error, response) => {
                                const date = new Date(response.body.time);
                                if (date.getTime() < userToResp.expiration_timeout_date) {
                                    const diff = Math.floor((userToResp.expiration_timeout_date - date.getTime()) / 1000);
                                    const minutes = Math.floor(diff / 60);
                                    let seconds = diff - minutes * 60;
                                    if (seconds < 10) {
                                        seconds = `0${seconds}`;
                                    }
                                    return reject(new Error(`Your account has been blocked for 10 seconds. Several login attempts from different PC (browsers). Minutes to unblocked ${minutes}:${seconds}`));
                                }
                                return resolve(end_point_db.any('update users set last_token = $1 where email = $2', [generatedToken, userToResp.email]));
                            });
                        });
                    }
                    return new Promise((resolve, reject) => {
                        request.get({
                            url: 'http://localhost:3002',
                            json: true,
                        }, (error, response) => {
                            let date = new Date(response.body.time);
                            date = new Date(date.getTime() + 10 * 1000);
                            return end_point_db.any('update users set expiration_timeout_date = $1 where email = $2', [date.getTime(), userToResp.email])
                                .then(() => end_point_db.any('update users set last_token = $1 where email = $2', [generatedToken, userToResp.email]))
                                .then(() => {
                                    res.clearCookie(WebAuth.CookieName);
                                    res.cookie(WebAuth.CookieName, generatedToken, {
                                        expire: new Date() + 60 * 10000000,
                                    });
                                    return reject(new Error('Your account has been blocked for 10 seconds. Several login attempts from different PC (browsers). Minutes to unblocked 0:10'));
                                });
                        });
                    });
                }
                return end_point_db.any('update users set last_token = $1 where email = $2', [generatedToken, userToResp.email]);
            })
            .then(() => {
                res.clearCookie(WebAuth.CookieName);
                res.cookie(WebAuth.CookieName, generatedToken, {
                    expire: new Date() + 60 * 10000000,
                });
                const response = {
                    user: {
                        id: userToResp.id,
                        email: userToResp.email,
                        is_registered: userToResp.is_registered,
                        is_admin: userToResp.is_admin,
                        subscription_type: userToResp.subscription_type,
                        expiration: userToResp.expiration_subscription_date,
                    },
                    cookie: generatedToken,
                };
                res.json(response);
            })
            .catch((error) => {
                res.statusCode = 409;
                res.json({
                    cookie: generatedToken,
                    message: error.message,
                });
            });
    } else {
        let authCookie = req.cookies[WebAuth.CookieName];
        if (!authCookie) {
            authCookie = req.body.auth;
        }
        if (authCookie) {
            verifyJwtToken(authCookie)
                .then(result => end_point_db.one('select * from users where users.email = $1', [result.email]))
                .then((user) => {
                    if (user.last_token !== authCookie) {
                        res.clearCookie(WebAuth.CookieName);
                        return Promise.reject(new Error('Someone use this account from other device. Relogin please'));
                    }
                    return new Promise((resolve, reject) => {
                        request.get({
                            url: 'http://localhost:3002',
                            json: true,
                        }, (error, resp) => {
                            const date = new Date(resp.body.time);
                            if (date.getTime() < user.expiration_timeout_date) {
                                const diff = Math.floor((user.expiration_timeout_date - date.getTime()) / 1000);
                                const minutes = Math.floor(diff / 60);
                                let seconds = diff - minutes * 60;
                                if (seconds < 10) {
                                    seconds = `0${seconds}`;
                                }
                                return reject(new Error(`Your account has been blocked for 10 seconds. Several login attempts from different PC (browsers). Minutes to unblocked ${minutes}:${seconds}`));
                            }
                            const response = {
                                user: {
                                    id: user.id,
                                    email: user.email,
                                    is_registered: user.is_registered,
                                    is_admin: user.is_admin,
                                    subscription_type: user.subscription_type,
                                    expiration: user.expiration_subscription_date,
                                },
                            };
                            res.clearCookie(WebAuth.CookieName);
                            res.cookie(WebAuth.CookieName, authCookie, {
                                expire: new Date() + 60 * 10000000,
                            });
                            res.json(response);
                        });
                    });
                })
                .catch((error) => {
                    res.statusCode = 409;
                    res.json({
                        message: error.message,
                    });
                });
        } else {
            res.statusCode = 409;
            if (req.body.oldAuth) {
                res.cookie(WebAuth.OldCookieName, req.body.oldAuth);
            }
            res.json({
                message: 'LOGIN ERROR: Empty credentials',
            });
        }
    }
});

app.post(`${reqPrefix}/logout`, (req, res) => {
    res.cookie(WebAuth.OldCookieName, req.cookies[WebAuth.CookieName], {
            expire: new Date() + 60 * 10000000,
        })
        .clearCookie(WebAuth.CookieName)
        .json({
            loggedOff: true,
            cookie: req.cookies[WebAuth.CookieName],
        });
});

app.get(`${reqPrefix}/invite_code/:user_id`, (req, res) => {
    const userId = req.params.user_id;
    let tokenToResp;
    if (userId) {
        end_point_db.any('select * from registration_tokens where user_id = $1 and status = \'free\'', [userId])
            .then((freeTokens) => {
                if (freeTokens && freeTokens.length > 0) {
                    tokenToResp = freeTokens[0].token;
                    end_point_db.any('update registration_tokens set status = \'pending\' where token = $1', [tokenToResp])
                        .then(() => {
                            res.json({
                                token: tokenToResp,
                            });
                        });
                } else {
                    end_point_db.any('select * from registration_tokens where user_id = $1 and status = \'pending\'', [userId])
                        .then((pendingTokens) => {
                            if (pendingTokens && pendingTokens.length > 0) {
                                tokenToResp = pendingTokens[0].token;
                                res.json({
                                    token: tokenToResp,
                                });
                            } else {
                                res.statusCode = 404;
                                res.json({
                                    message: 'You already gived all tokens',
                                });
                            }
                        });
                }
            });
    } else {
        res.statusCode = 409;
        res.json({
            message: 'LOGIN ERROR: Empty credentials',
        });
    }
});

app.post(`${reqPrefix}/change_password`, isAuth, (req, res) => {
    const {
        email,
        changePasswordModel,
    } = req.body;
    const {
        oldPassword,
        password,
        confirm,
    } = changePasswordModel;
    let user;

    end_point_db.one('select * from users where users.email = $1', [email])
        .then((result) => {
            if (password !== confirm) {
                return Promise.reject(new Error('password and confirm are not same!'));
            }
            user = result;
            return encrypt(oldPassword, user.salt);
        })
        .then((encryptObj) => {
            const {
                generatedPassword,
            } = encryptObj;
            if (generatedPassword === user.password) {
                return encrypt(password, user.salt);
            }
            return Promise.reject(new Error('Wrong password'));
        })
        .then((encryptObj) => {
            const {
                generatedPassword,
            } = encryptObj;
            return end_point_db.any('update users set password = $1 where email = $2', [generatedPassword, user.email]);
        })
        .then(() => {
            res.clearCookie(WebAuth.CookieName);
            res.json({
                loggedOff: true,
            });
        })
        .catch((error) => {
            res.statusCode = 409;
            res.json({
                message: error.message,
            });
        });
});

app.get(`${reqPrefix}/admin/users`, isAdmin, async (req, res) => {
    const {
        page,
        pageSize,
        nickname,
        isShowActive,
        isShowAdmins,
        typeToSearch,
    } = req.query;

    const offset = (page - 1) * pageSize;

    const response = await fetch('http://localhost:3002');
    const json = await response.json();

    const {
        time,
    } = json;
    const activeQuery = isShowActive ? 'and users.subscription_type IS NOT null and users.expiration_subscription_date > $4' : '';
    const adminQuery = isShowAdmins ? 'and users.is_admin = true' : '';
    const typeQuery = typeToSearch ? 'and users.subscription_type = $5' : '';


    const {
        count,
    } = await end_point_db.one(
        `select count(*) from users where email like $1 ${activeQuery} ${adminQuery} ${typeQuery}`,
        [`${nickname}%`, null, null, time, +typeToSearch],
    );

    const users = await end_point_db.many(
            `select users.id,users.is_unlimit, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
            (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
            left join(
            select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
            where users.email like $1 ${activeQuery} ${adminQuery} ${typeQuery}
            order by id limit $2 offset $3`,
            [`${nickname}%`, pageSize, offset, time, +typeToSearch],
        )
        .catch(() => {});


    const {
        count: activeCount,
    } = await end_point_db.one(
        `select count(*) from users where subscription_type IS NOT null and expiration_subscription_date > $1 ${adminQuery} ${typeQuery}`,
        [time, null, null, null, +typeToSearch],
    );

    if (users) {
        res.json({
            users,
            count,
            activeCount,
        });
    } else {
        res.statusCode = 405;
        res.json({
            message: 'no users to this request',
        });
    }
});

app.get(`${reqPrefix}/admin/user-info`, async (req, res) => {
    const {
        email,
        isShowActive,
        isShowAdmins,
        typeToSearch,
    } = req.query;

    const activeQuery = isShowActive ? 'and users.subscription_type IS NOT null and users.expiration_subscription_date > $2' : '';
    const adminQuery = isShowAdmins ? 'and users.is_admin = true' : '';
    const typeQuery = typeToSearch ? 'and users.subscription_type = $3' : '';

    const response = await fetch('http://localhost:3002');
    const json = await response.json();

    const {
        time,
    } = json;

    const invites = await end_point_db.many(`
        select users.id,users.is_unlimit, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter from registration_tokens 
        join users on registration_tokens.token = users.invited_by_key 
        join users as a on registration_tokens.user_id = a.id 
        where a.email = $1 ${activeQuery} ${adminQuery} ${typeQuery}
    `, [email, time, +typeToSearch])
        .catch(() => {
            res.statusCode = 404;
            res.json({
                message: 'no user with such email or zero users finded',
            });
        });

    if (invites) {
        res.json({
            email,
            invites,
        });
    }
});


app.post(`${reqPrefix}/admin/subscribe`, isAdmin, (req, res) => {
    const {
        id,
        type,
        days,
    } = req.body;

    if (days >= 0) {
        request.get({
            url: 'http://localhost:3002',
            json: true,
        }, (error, response) => {
            const date = new Date(response.body.time);
            date.setDate(date.getDate() + days);

            end_point_db.any(`update users 
                set number_of_searches = $1,
                 subscription_type = $2,
                 expiration_subscription_date = $3 where id = $4`, [numberOfSearches[type], type, date.getTime(), id])
                .then(() => end_point_db.one(`select users.id, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter from users
                left join(
                select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
                where users.id= $1`, [id]))
                .then((result) => {
                    res.json(result);
                })
                .catch(() => {
                    res.status = 405;
                    res.json({
                        error: 'somthing wrong',
                    });
                });
        });
    } else {
        res.statusCode = 409;
        res.json({
            message: 'Enter correct days',
        });
    }
});

app.post(`${reqPrefix}/admin/create_invite_code`, isAdmin, (req, res) => {
    const {
        count,
        id,
    } = req.body;
    let promises = [];

    for (let i = 0; i < count; i += 1) {
        promises.push(generateRegistrationTokenToken());
    }

    Promise.all(promises)
        .then((tokens) => {
            promises = [];
            for (const token of tokens) {
                promises.push(end_point_db.any('insert into registration_tokens (token, status, user_id) VALUES ($2, \'free\', $1)', [id, token]));
            }

            return Promise.all(promises);
        })
        .then(() => end_point_db.one(`select users.id, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
        (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
        left join(
        select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
        where users.id= $1`, [id]))
        .then((result) => {
            res.json(result);
        })
        .catch(() => {
            res.statusCode = 405;
            res.json({
                error: 'something wrong',
            });
        });
});

app.post(`${reqPrefix}/admin/make_admin`, isAdmin, (req, res) => {
    const {
        id,
    } = req.body;

    end_point_db.any('update users set is_admin = $1 where id = $2', [true, id])
        .then(() => end_point_db.one(`select users.id, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
        (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
        left join(
        select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
        where users.id= $1`, [id]))
        .then((result) => {
            res.json(result);
        })
        .catch(() => {
            res.status = 405;
            res.json({
                message: 'something wrong',
            });
        });
});

app.post(`${reqPrefix}/admin/make_simple_user`, isAdmin, (req, res) => {
    const {
        id,
    } = req.body;

    end_point_db.any('update users set is_admin = $1 where id = $2', [false, id])
        .then(() => end_point_db.one(`select users.id, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
        (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
        left join(
        select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
        where users.id = $1`, [id]))
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.status = 405;
            res.json({
                message: 'something wrong',
            });
        });
});

app.post(`${reqPrefix}/admin/make_unlimit`, isAdmin, (req, res) => {
    const {
        id,
    } = req.body;

    end_point_db.any('update users set is_unlimit = $1 where id = $2', [true, id])
        .then(() => end_point_db.one(`select users.id, users.is_unlimit, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
        (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
        left join(
        select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
        where users.id= $1`, [id]))
        .then((result) => {
            res.json(result);
        })
        .catch(() => {
            res.status = 405;
            res.json({
                message: 'something wrong',
            });
        });
});

app.post(`${reqPrefix}/admin/make_limit`, isAdmin, (req, res) => {
    const {
        id,
    } = req.body;

    end_point_db.any('update users set is_unlimit = $1 where id = $2', [false, id])
        .then(() => end_point_db.one(`select users.id,users.is_unlimit, users.email, users.expiration_subscription_date, users.subscription_type, users.is_admin, a.email as inviter,
        (select count(*) from registration_tokens where user_id = users.id and status='free') as count_free_tokens from users
        left join(
        select * from registration_tokens join users on registration_tokens.user_id = users.id) as a on users.invited_by_key = a.token
        where users.id = $1`, [id]))
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.status = 405;
            res.json({
                message: 'something wrong',
            });
        });
});

// const transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');

app.post(`${reqPrefix}/reset-password`, (req, res) => {
    const {
        email,
    } = req.body;
    const password = keygen._({
        length: 12,
        specials: true,
    });

    let user;
    end_point_db.one('select * from users where email = $1', [email])
        .then((u) => {
            user = u;
            return encrypt(password, user.salt);
        })
        .then((encryptObj) => {
            const {
                generatedPassword,
            } = encryptObj;
            return end_point_db.any('update users set password = $1 where email = $2', [generatedPassword, user.email]);
        })
        .then(() => {
            const mailOptions = {
                from: process.env.MAIL_LOGIN, // sender address
                to: `${email}`, // list of receivers
                subject: 'Change password', // Subject line
                text: 'password', // plain text body
                html: `<b>email:${email} new password:${password}</b>`, // html body
            };

            // send mail with defined transport object
            let emailMessage = '';
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    emailMessage = `there was an error :-(, and it was this: ${error.message}`;
                } else {
                    emailMessage = `Message sent: ${info.response}`;
                }

                res.json({
                    message: 'success',
                    email: emailMessage,
                });
            });
        })
        .catch(() => {
            res.statusCode = 404;
            res.json({
                error: 'not found',
            });
        });
});

async function updateUsersSearches(users) {
    for (const user of users) {
        const {
            expiration_subscription_date,
            subscription_type,
            id,
        } = user;
        await new Promise((resolve) => {
            request.get({
                url: 'http://localhost:3002',
                json: true,
            }, (error, response) => {
                if (expiration_subscription_date &&
                    parseInt(expiration_subscription_date, 10) > response.body.time) {
                    end_point_db.any(`update users 
                            set number_of_searches = $1 where id = $2`, [numberOfSearches[subscription_type], id])
                        .then(() => resolve());
                } else {
                    end_point_db.any(`update users 
                            set subscription_type = $3,
                            expiration_subscription_date = $4,
                            number_of_searches = $1 where id = $2`, [0, id, null, null])
                        .then(() => resolve());
                }
            });
        });
    }
    return Promise.resolve();
}

function recalculateSubscription() {
    const now = new Date();
    let millisTill =
        new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0) - now;
    if (millisTill < 0) {
        millisTill += 86400000;
    }

    setTimeout(() => {
        end_point_db.many('select * from users')
            .then(users => updateUsersSearches(users))
            .then(() => {
                recalculateSubscription();
            });
    }, millisTill);
}


// end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'')
//     .then((formatedTables) => {
//         console.log(formatedTables.map(x => x.table_name));
//         let compiledQuery = '';

//         for (let i = 0; i < formatedTables.length; i += 1) {
//             const playerresultsTable = formatedTables[i];
//             const tableName = playerresultsTable.table_name;

//             const query = `select * from ${tableName}`;
//             if (i + 1 !== formatedTables.length) {
//                 compiledQuery = `${compiledQuery} ${query} union all`;
//             } else {
//                 compiledQuery = `${compiledQuery} ${query}`;
//             }
//         }

//         console.log(compiledQuery);
//     });


async function init() {
    await new Promise(() => {
        request.get({
            url: 'http://localhost:3002',
            json: true,
        }, (error, response) => {
            if (error) {
                console.log(chalk.error('Hand4hand API couldn\'t started. Error: ', error));
            } else {
                console.log(chalk.green('Time server working!!! Time server time:', new Date(response.body.time).toString()));

                const port = process.env.PORT || 3001;
                const server = process.env.SERVER || '127.0.0.1';

                app.listen(port, server).on('error', console.log);
                console.log(chalk.cyan(new Date().toString()), chalk.green('Hand4hand API started at port:', port));
            }
        });
    });
}

recalculateSubscription();
init();
