export function isUserHaveSearches(numberOfSearches) {
    if (typeof numberOfSearches !== 'number') {
        return false;
    }

    if (numberOfSearches <= 0) {
        return false;
    }

    return true;
}

export function isUserCanSearchThisLimit(limit, subscriptionType, homeLimit) {
    if (typeof subscriptionType !== 'number') {
        return false;
    }

    if ((limit === undefined || limit === null) && (homeLimit === null)) {
        return true;
    }

    let l;

    let allowedType;
    if (limit && homeLimit) {
        if (+limit > homeLimit) {
            l = limit;
        } else {
            l = homeLimit;
        }

        if (l >= 0 && l <= 16) {
            allowedType = 0;
        }
        if (l > 16 && l <= 50) {
            allowedType = 1;
        }
        if (l > 50) {
            allowedType = 2;
        }
    } else {
        if (limit) {
            if (limit >= 0 && limit <= 16) {
                allowedType = 0;
            }
            if (limit > 16 && limit <= 50) {
                allowedType = 1;
            }
            if (limit > 50) {
                allowedType = 2;
            }
        }

        if (homeLimit) {
            if (homeLimit >= 0 && homeLimit <= 16) {
                allowedType = 0;
            }
            if (homeLimit > 16 && homeLimit <= 50) {
                allowedType = 1;
            }
            if (homeLimit > 50) {
                allowedType = 2;
            }
        }
    }


    if (subscriptionType >= allowedType) {
        return true;
    }

    return false;
}

export function generateApiError(message, statusCode) {
    const error = new Error(message);
    error.statusCode = statusCode || 404;
    return error;
}

export function isReqWithoutFilters(pokerGameType, limit, tableSize, date) {
    if (!pokerGameType && !limit && !tableSize && !date) {
        return true;
    }
    return false;
}
