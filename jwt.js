import { sign, verify } from 'jsonwebtoken';
import keygen from 'keygenerator';
import { end_point_db } from './server';

const Jwt = {
    Options: {
        expiresIn: '355 days',
    },
    Secret: 'SECRET',
};

const JwtRegistration = {
    Options: {
    },
    Secret: 'SECRET',
};

const registrationTokensCount = 3;

export function generateJwtToken(loginModel) {
    return new Promise((resolve, reject) => {
        const tokenData = {
            email: loginModel.email,
        };

        sign(tokenData, Jwt.Secret, Jwt.Options, (error, token) => {
            if (error) {
                return reject(error);
            }
            return resolve(token);
        });
    });
}

export function verifyJwtToken(token) {
    return new Promise((resolve, reject) => {
        verify(token, Jwt.Secret, (error, decoded) => {
            if (error) { return reject(error); }

            return resolve(decoded);
        });
    });
}


export function generateRegistrationTokenToken() {
    const key = keygen._({
        length: 12,
        specials: true,
    });
    return end_point_db.any('select * from registration_tokens where token = $1', [key])
        .then((results) => {
            if (results && results.length === 1) {
                return generateRegistrationTokenToken();
            }
            return Promise.resolve(key);
        });

    // return new Promise((resolve, reject) => {
    //     const tokenData = {
    //         email,
    //         number,
    //     };

    //     sign(tokenData, JwtRegistration.Secret, JwtRegistration.Options, (error, token) => {
    //         if (error) {
    //             return reject(error);
    //         }
    //         return resolve(token);
    //     });
    // });
}

export function generateThreeRegistrationTokens(email) {
    const promises = [];
    for (let i = 0; i < registrationTokensCount; i += 1) {
        promises.push(generateRegistrationTokenToken(email, i));
    }
    return Promise.all(promises);
}

export function verifyRegistrationToken(token) {
    return new Promise((resolve, reject) => {
        verify(token, JwtRegistration.Secret, (error, decoded) => {
            if (error) { return reject(new Error('Wrong invition code')); }

            return resolve(decoded);
        });
    });
}
