import speakeasy from 'speakeasy';
import { makeSalt, encrypt } from '../register';

const { generateThreeRegistrationTokens } = require('../jwt');

const pgp = require('pg-promise')({

});

const cn = {
    host: 'localhost', // 'localhost' is the default;
    port: 5432, // 5432 is the default;
    database: 'fix-graph-end',
    user: 'postgres',
    password: 'qwerty',
};

const db = pgp(cn);

const admin = {
    email: 'admin@admin.ru',
    password: 'admin',
};

db.any('drop table users')
    .then(() => db.any(`create table if not exists registration_tokens(
    token text primary key,
    status text,
    user_id integer 
)`))

    .then(() => db.any(`create table if not exists users(
        id serial primary key,
        email text,
        password text,
        salt text,
        auth_secret text,
        is_registered boolean,
        is_admin boolean,
        expiration_subscription_date bigint,
        number_of_searches integer,
        subscription_type integer,
        last_token text,
        invited_by_key text
    )`))
    .then(() => db.any('delete from users'))
    .then(() => {
        console.log('deleted users');
        return db.any('delete from registration_tokens');
    })
    .then(() => {
        console.log('deleted registration_tokens');
        return makeSalt();
    })
    .then(salt => encrypt(admin.password, salt))
    .then((encryptObj) => {
        const { generatedPassword, salt } = encryptObj;
        const secret = speakeasy.generateSecret({ length: 20 });
        return db.any(
            `insert into users 
                (id, email, password, salt, auth_secret, is_registered, is_admin)
                 VALUES ($1, $2, $3, $4, $5, $6, $7)`,
            [1, admin.email, generatedPassword, salt, secret.base32, true, true],
        );
    })
    .then(() => {
        console.log('admin created');
        return generateThreeRegistrationTokens(admin.email);
    })
    // .then((tokens) => {
    //     console.log(tokens);
    //     return db.any(
    //         `insert into registration_tokens (token, status, user_id)
    //          VALUES
    //          ($2, 'free', $1),
    //          ($3, 'free', $1),
    //          ($4, 'free', $1)`,
    //         [1, tokens[0], tokens[1], tokens[2]],
    //     );
    // })
    .then(() => {
        console.log('end');
        process.exit();
    });
