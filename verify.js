import speakeasy from 'speakeasy';
import { end_point_db } from './server';

export function verifyToken(email, token) {
    return end_point_db.one('select * from users where users.email = $1', [email])
        .then((user) => {
            const secret = user.auth_secret;
            const verified = speakeasy.totp.verify({
                secret,
                encoding: 'base32',
                token,
            });

            if (verified) {
                return Promise.resolve(user);
            }
            return Promise.reject(new Error('Wrong token, try again'));
        });
}
