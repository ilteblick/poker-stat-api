import speakeasy from 'speakeasy';
import crypto from 'crypto';

import {end_point_db,} from './server';
import {verifyRegistrationToken,} from './jwt';

const defaultIterations = 10000;
const defaultKeyLength = 64;
const bufferEncodingValue = 'base64';
const pbkdf2Algorithm = 'sha512';

export function makeSalt() {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, salt) => {
            if (err) {
                reject(err);
            } else {
                const generatedSalt = salt.toString(bufferEncodingValue);
                resolve(generatedSalt);
            }
        });
    });
}

export function encrypt(password, salt) {
    return new Promise((resolve, reject) => {
        const realSalt = Buffer.from(salt, bufferEncodingValue);
        crypto.pbkdf2(
            password, realSalt, defaultIterations,
            defaultKeyLength, pbkdf2Algorithm, (err, derivedKey) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({
                        generatedPassword: derivedKey.toString(bufferEncodingValue),
                        salt,
                    });
                }
            },
        );
    });
}


// export function register(registerModel) {
//     const {
//         email, password, token,
//     } = registerModel;
//     let otpauth_url;
//     let key;
//     return end_point_db.any('select * from registration_tokens where token = $1 and status = \'pending\'', [token])
//         // return verifyRegistrationToken(token)
//         // .then(() => end_point_db.any('select * from registration_tokens where token = $1 and status = \'pending\'', [token]))
//         // .then(() => end_point_db.any('select * from registration_tokens where token = $1 and status = \'free\'', [token]))
//         .then(async (result) => {
//             if (result && result.length === 0) {
//                 const bonusToken = await end_point_db.one('select * from registration_tokens where token = $1 and is_bonus = $2', [token, true])
//                     .catch(() => {

//                     });
//                 if (bonusToken) {
//                     if (bonusToken.bonus_count > 0) {
//                         key = bonusToken.token;
//                         await end_point_db.any('update registration_tokens set bonus_count = bonus_count - 1 where token = $1', [token]);
//                         return Promise.resolve();
//                     }
//                 }
//                 return Promise.reject(new Error('This invition code is alredy used'));
//             }
//             key = result[0].token;
//             return end_point_db.any('update registration_tokens set status = \'used\' where token = $1', [token]);
//         })
//         .then(() => end_point_db.any('select * from users where users.email = $1', [email]))
//         // TODO добавить проверки
//         .then((result) => {
//             if (result && result.length === 0) {
//                 return makeSalt();
//             }
//             return Promise.reject(new Error('This email already used.'));
//         })
//         .then(salt => encrypt(password, salt))
//         .then((encryptObj) => {
//             const { generatedPassword, salt } = encryptObj;
//             const secret = speakeasy.generateSecret({ length: 20 });
//             const confiramtionToken = speakeasy.generateSecret({ length: 32 });
//             otpauth_url = secret.otpauth_url;
//             return end_point_db.any(
//                 'insert into users (email, password, salt, auth_secret, is_registered, is_admin, invited_by_key, confirmation_token) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)',
//                 [email, generatedPassword, salt, secret.base32, false, false, key, confiramtionToken.base32],
//             );
//         })
//         .then(() => end_point_db.one('select * from users where users.email = $1', [email]))
//         .then(result => Promise.resolve({
//             user: result,
//             otpauth_url,
//         }));
// }

export async function register(registerModel) {
    const {
        email,
        password,

    } = registerModel;
    let token = registerModel.token;
    let key = null;

    if (token === '') {
        token = null;
    }

    const existingUser = await end_point_db.one('select * from users where users.email = $1', [email])
        .catch(() => {});

    if (existingUser) {
        return Promise.reject(new Error('This email already used.'));
    }

    if (token) {
        const tokenFromDb = await end_point_db.one('select * from registration_tokens where token = $1 and status = \'pending\'', [token])
            .catch(() => {});

        if (!tokenFromDb) {
            const bonusToken = await end_point_db.one('select * from registration_tokens where token = $1 and is_bonus = $2', [token, true])
                .catch(() => {});
            if (bonusToken && bonusToken.bonus_count > 0) {
                key = bonusToken.token;
                await end_point_db.any('update registration_tokens set bonus_count = bonus_count - 1 where token = $1', [token]);
            } else {
                return Promise.reject(new Error('This invition code is alredy used'));
            }
        } else {
            key = tokenFromDb.token;
            await end_point_db.any('update registration_tokens set status = \'used\' where token = $1', [token]);
        }
    }

    const salt = await makeSalt();
    const encryptObj = await encrypt(password, salt);
    const {
        generatedPassword,
    } = encryptObj;
    const secret = speakeasy.generateSecret({
        length: 20,
    });
    const confiramtionToken = speakeasy.generateSecret({
        length: 32,
    });
    const otpauth_url = secret.otpauth_url;

    await end_point_db.any(
        `insert into users (email, password, salt, auth_secret, is_registered, is_admin, invited_by_key, confirmation_token, is_unlimit, subscription_type, expiration_subscription_date) 
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
        [email, generatedPassword, salt, secret.base32, false, false, key, confiramtionToken.base32, true, 2, new Date(2021, 1, 1, 0, 0, 0, 0).getTime()],
    );

    const createdUser = await end_point_db.one('select * from users where users.email = $1', [email]);
    return {
        user: createdUser,
        otpauth_url,
    };
}
