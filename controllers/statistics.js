import StatisticsService from '../services/statistics';
import { end_point_db } from '../server';

async function checkPermissions(req, res) {
    const {
        playername,
        pokersite_id,
        limit,
    } = req.query;

    StatisticsService.checkPermissions(req.user, playername, pokersite_id, limit)
        .then(() => {
            res.json({
                check: true,
            });
        })
        .catch(({
            message,
            statusCode,
        }) => {
            res.statusCode = statusCode || 404;
            res.json({
                error: message,
                check: false,
            });
        });
}

async function getAgrFactorStats(req, res) {
    const {
        playername,
        pokerGameType,
        limit,
        tableSize,
        date,
        pokersite_id,
    } = req.query;

    StatisticsService.checkPermissions(req.user, playername, pokersite_id, limit)
        .then(player => StatisticsService.getAgrFactorStats(playername, pokerGameType, limit, tableSize, date, pokersite_id, player))
        .then((result) => {
            res.json(result);
        })
        .catch(({
            message,
            statusCode,
        }) => {
            res.statusCode = statusCode;
            res.json({
                message,
                check: false,
            });
        });
}

async function getBaseStats(req, res) {
    const {
        playername,
        pokerGameType,
        limit,
        tableSize,
        date,
        pokersite_id,
    } = req.query;

    // end_point_db.any('update users set number_of_searches = number_of_searches - 1 where email = $1', [req.user.email])
        // .then(() => StatisticsService.checkPermissions(req.user, playername, pokersite_id, limit))
        end_point_db.any('select * from players where playername = $1', [playername])
        .then(([player]) => StatisticsService.getBaseStats(playername, pokerGameType, limit, tableSize, date, pokersite_id, player))
        .then((result) => {
            res.json(result);
        })
        .catch(({
            message,
            statusCode,
        }) => {
            res.statusCode = statusCode;
            res.json({
                message,
                check: false,
            });
        });
}

async function getGraphStats(req, res) {
    const {
        playername,
        pokerGameType,
        limit,
        tableSize,
        date,
        pokersite_id,
    } = req.query;

    StatisticsService.checkPermissions(req.user, playername, pokersite_id, limit)
        .then(player => StatisticsService.getGraphStats(playername, pokerGameType, limit, tableSize, date, pokersite_id, player))
        .then((result) => {
            res.json(result);
        })
        .catch(({
            message,
            statusCode,
        }) => {
            res.statusCode = statusCode;
            res.json({
                message,
                check: false,
            });
        });
}

export default {
    checkPermissions,
    getAgrFactorStats,
    getBaseStats,
    getGraphStats,
};
